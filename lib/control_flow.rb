# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |char|
    str.delete! (char) if char == char.downcase
  end
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  strlen = str.length
  mid = strlen / 2
  strlen.even? ? str[mid-1..mid] : str[mid]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count "aeiou"
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joint_string = ""
  arr.each_with_index do |word, idx|
    if idx == arr.length-1
      joint_string << word
    else
      joint_string << word + separator
    end
  end

  joint_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_string = ""

  str.each_char.with_index do |char, idx|
    idx.odd? ? weird_string << char.upcase :
    weird_string << char.downcase
  end

  weird_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(" ")

  words.reduce([]) do |arr, words|
    words.length >= 5 ? arr << words.reverse : arr << words
  end.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |num|
    if num % 15 == 0
      "fizzbuzz"
    elsif num % 5 == 0
      "buzz"
    elsif num % 3 == 0
      "fizz"
    else
      num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2

  (2...num).each do |numbs|
    return false if num % numbs == 0
  end

  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).reduce([]) do |factors, numbs|
    num % numbs == 0 ? factors << numbs : factors
  end
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).reduce([]) do |acc, numbs|
    prime?(numbs) ? acc << numbs : acc
  end
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = Array.new
  odds = Array.new

  arr.each do |num|
    num.even? ? evens << num : odds << num
  end

  return evens[0] if evens.count == 1
  odds[0]
end
